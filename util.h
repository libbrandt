/* This file is part of libbrandt.
 * Copyright (C) 2016 GNUnet e.V.
 *
 * libbrandt is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * libbrandt is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * libbrandt.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file util.h
 * @brief Interface for the common utility functions.
 * @author Markus Teich
 */

#ifndef _BRANDT_UTIL_H
#define _BRANDT_UTIL_H


#define brandt_assert(cond) GNUNET_assert_from (cond, "libbrandt")


#ifdef NDEBUG

#define DP(point) ((void)(gcry_log_debugpnt (# point, point, ec_ctx)))
#define DM(mpi)   ((void)(gcry_log_debugmpi (# mpi, mpi)))
#define DS(sexp)  ((void)(gcry_log_debugsxp (# sexp, sexp)))

#else /* ifdef NDEBUG */

#define DP(point) ((void)(0))
#define DM(mpi)   ((void)(0))
#define DS(sexp)  ((void)(0))

#endif /* ifdef NDEBUG */


#endif /* ifndef _BRANDT_UTIL_H */
