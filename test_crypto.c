/* This file is part of libbrandt.
 * Copyright (C) 2016 GNUnet e.V.
 *
 * libbrandt is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * libbrandt is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * libbrandt.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file test_crypto.c
 * @brief testing crypto and smc functions.
 * @author Markus Teich
 */

#include "platform.h"

/* For testing static functions and variables we include the whole source */
#include "crypto.c"

#include "test.h"


static int
test_serialization ()
{
	gcry_mpi_point_t oldp = gcry_mpi_point_new (0);
	gcry_mpi_point_t newp = gcry_mpi_point_new (0);
	gcry_mpi_t       oldi = gcry_mpi_new (256);
	gcry_mpi_t       newi = gcry_mpi_new (256);
	struct ec_mpi    serp;
	struct ec_mpi    seri;

	ec_keypair_create (oldp, oldi);

	ec_point_serialize (&serp, oldp);
	mpi_serialize (&seri, oldi);

	ec_point_parse (newp, &serp);
	mpi_parse (newi, &seri);

	CHECK (!ec_point_cmp (oldp, newp), "serialization changed point");
	CHECK (!gcry_mpi_cmp (oldi, newi), "serialization changed mpi");

	mpi_serialize (&seri, GCRYMPI_CONST_ONE);
	mpi_parse (newi, &seri);
	CHECK (!gcry_mpi_cmp (GCRYMPI_CONST_ONE, newi), "serializing mpi 1 fail");

	gcry_mpi_point_release (oldp);
	gcry_mpi_point_release (newp);
	gcry_mpi_release (oldi);
	gcry_mpi_release (newi);
	return 1;
}


static int
test_smc_zkp_dl ()
{
	struct proof_dl  proof;
	gcry_mpi_t       x = gcry_mpi_new (256);
	gcry_mpi_point_t v = gcry_mpi_point_new (0);

	ec_skey_create (x);

	smc_zkp_dl (v, x, &proof);
	CHECK (gcry_mpi_ec_curve_point (v, ec_ctx), "not on curve");
	CHECK (!smc_zkp_dl_check (v, &proof), "zkp dl wrong");

	gcry_mpi_release (x);
	gcry_mpi_point_release (v);
	return 1;
}


static int
test_smc_zkp_2dle ()
{
	struct proof_2dle proof;
	gcry_mpi_t        x = gcry_mpi_new (256);
	gcry_mpi_point_t  g1 = gcry_mpi_point_new (0);
	gcry_mpi_point_t  g2 = gcry_mpi_point_new (0);
	gcry_mpi_point_t  v = gcry_mpi_point_new (0);
	gcry_mpi_point_t  w = gcry_mpi_point_new (0);

	ec_keypair_create (g1, x);
	ec_keypair_create (g2, x);

	smc_zkp_2dle (v, w, g1, g2, x, &proof);
	CHECK (gcry_mpi_ec_curve_point (g1, ec_ctx), "not on curve");
	CHECK (gcry_mpi_ec_curve_point (g2, ec_ctx), "not on curve");
	CHECK (gcry_mpi_ec_curve_point (v, ec_ctx), "not on curve");
	CHECK (gcry_mpi_ec_curve_point (w, ec_ctx), "not on curve");
	CHECK (!smc_zkp_2dle_check (v, w, g1, g2, &proof), "zkp 2dle wrong");

	gcry_mpi_release (x);
	gcry_mpi_point_release (g1);
	gcry_mpi_point_release (g2);
	gcry_mpi_point_release (v);
	gcry_mpi_point_release (w);
	return 1;
}


static int
test_smc_zkp_0og ()
{
	struct proof_0og proof;
	gcry_mpi_point_t y = gcry_mpi_point_new (0);
	gcry_mpi_point_t alpha = gcry_mpi_point_new (0);
	gcry_mpi_point_t beta = gcry_mpi_point_new (0);

	/* get random public key point. We don't need the secret key to check the
	 * proof here */
	ec_keypair_create (y, NULL);

	smc_zkp_0og (tests_run % 2, y, NULL, alpha, beta, &proof);
	CHECK (gcry_mpi_ec_curve_point (alpha, ec_ctx), "not on curve");
	CHECK (gcry_mpi_ec_curve_point (beta, ec_ctx), "not on curve");
	CHECK (!smc_zkp_0og_check (y, alpha, beta, &proof), "zkp 0og is wrong");

	gcry_mpi_point_release (y);
	gcry_mpi_point_release (alpha);
	gcry_mpi_point_release (beta);
	return 1;
}


int
main (int argc, char *argv[])
{
	int                                 repeat = 1;
	struct GNUNET_CRYPTO_EccDlogContext *edc;

	if (GNUNET_OK != GNUNET_log_setup ("test_crypto", "WARNING", NULL))
		return 1;

	edc = GNUNET_CRYPTO_ecc_dlog_prepare (1024, 16);
	BRANDT_init (edc);

	/* tests that need to run only once */
	RUN (test_serialization);

	for (tests_run = 0; tests_run < repeat; tests_run++)
	{
		RUN (test_smc_zkp_dl);
		RUN (test_smc_zkp_2dle);
		RUN (test_smc_zkp_0og);
	}

	GNUNET_CRYPTO_ecc_dlog_release (edc);
	return ret;
}
