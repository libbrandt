/* This file is part of libbrandt.
 * Copyright (C) 2016 GNUnet e.V.
 *
 * libbrandt is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * libbrandt is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * libbrandt.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file platform.h
 * @brief This file was adapted from TALER and contains some magic
 * configuration. It should be included in every compilation as the first
 * include directive, since it sets up defines which are used in other includes.
 * @author Markus Teich
 */

#ifndef PLATFORM_H_
#define PLATFORM_H_

/* Include our configuration header */
#ifndef HAVE_USED_CONFIG_H
# define HAVE_USED_CONFIG_H
# ifdef HAVE_CONFIG_H
#  include "brandt_config.h"
# endif /* ifdef HAVE_CONFIG_H */
#endif  /* ifndef HAVE_USED_CONFIG_H */

/* Include GNUnet's platform file */
#include <gnunet/platform.h>

/* Do not use shortcuts for gcrypt mpi */
#define GCRYPT_NO_MPI_MACROS 1

/* Do not use deprecated functions from gcrypt */
#define GCRYPT_NO_DEPRECATED 1

#endif  /* PLATFORM_H_ */

/* end of platform.h */
