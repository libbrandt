/* This file is part of libbrandt.
 * Copyright (C) 2016 GNUnet e.V.
 *
 * libbrandt is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * libbrandt is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * libbrandt.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file internals.h
 * @brief This header contains library internal structs.
 * @author Markus Teich
 */

#ifndef _BRANDT_INTERNALS_H
#define _BRANDT_INTERNALS_H

#include <gcrypt.h>

#include "brandt.h"


enum rounds {
	msg_init = 0,
	msg_bid = 1,
	msg_outcome = 2,
	msg_decrypt = 3,
	msg_last = 4
};


enum auction_type {
	auction_firstPrice = 0,
	auction_mPlusFirstPrice = 1,
	auction_last = 2
};


enum outcome_type {
	outcome_private = 0,
	outcome_public = 1,
	outcome_last = 2
};


GNUNET_NETWORK_STRUCT_BEGIN

/**
 * Every message is prefixed with this header.
 * 
 * BEWARE: Introducing a 16bit length field will not work. M+1st price auctions
 * with private outcome send a n*n*(n-1)*k*128 = n²(n-1)*64 (for a reasonable
 * k-choice of 512 possible prices) byte message from the seller to all bidders
 * (decrypt-round). For just two bidders this is already 256k bytes, but 16bit
 * uint only can address 64k bytes. */
struct msg_head {
	uint32_t prot_version GNUNET_PACKED;
	uint32_t msg_type     GNUNET_PACKED;
};


/**
 * This struct describes the parameters of an auction. All fields are stored in
 * network byte order.
 */
struct BRANDT_DescrP {
	/** Starting time of the auction. Bidders have to join the auction via
	 * BRANDT_join until this time */
	struct GNUNET_TIME_AbsoluteNBO time_start;

	/** The maximum duration the participants have to complete each round. */
	struct GNUNET_TIME_RelativeNBO time_round;

	/** The amount of possible prices */
	uint16_t k GNUNET_PACKED;

	/** Auction type. 0 means first price Auction, >= 0 means M+1st price
	 * auction with an amount of m items being sold. */
	uint16_t m GNUNET_PACKED;

	/** Outcome type. 0 means private outcome, everything else means public
	 * outcome. */
	uint16_t outcome_public GNUNET_PACKED;

	/** reserved for future use. Must be zeroed out. */
	uint16_t reserved GNUNET_PACKED;
};

GNUNET_NETWORK_STRUCT_END


struct BRANDT_Auction {
	/** Starting time of the auction. Bidders have to join the auction via
	 * BRANDT_join until this time */
	struct GNUNET_TIME_Absolute time_start;

	/** The maximum duration the participants have to complete each round. */
	struct GNUNET_TIME_Relative time_round;

	/** Auction type. 0 means first price Auction, >= 0 means M+1st price
	 * auction with an amount of m items being sold. */
	uint16_t m;

	/** Outcome type. 0 means private outcome, everything else means public
	 * outcome. */
	uint16_t outcome_public;

	/** Link to the next delayed task (auction start trigger, round trigger) */
	struct GNUNET_SCHEDULER_Task *task;

	void *closure;              /** auction closure given by the user */

	BRANDT_CbResult  result;    /** result reporting callback */
	BRANDT_CbDeliver bcast;     /** broadcast callback */
	BRANDT_CbDeliver ucast;     /** unicast callback */
	BRANDT_CbStart   start;     /** start callback */

	int         seller_mode;    /** If 0 we are bidding, selling otherwise */
	enum rounds cur_round;      /** The round we expect messages from */
	gcry_mpi_t  round_progress; /** Stores which round messages were received */

	uint16_t n;                 /** The amount of bidders/agents */
	uint16_t k;                 /** The amount of possible prices */
	uint16_t i;                 /** Own agents index, only used when bidding */
	uint16_t b;                 /** Own bid */

	gcry_mpi_t       x;         /** Own private additive key share */
	gcry_mpi_point_t *y;        /** public multiplicative key shares, size: n */
	gcry_mpi_point_t Y;         /** Shared public key */

	gcry_mpi_point_t **alpha;   /** alphas, size: n*k */
	gcry_mpi_point_t **beta;    /** betas, size: n*k */

	gcry_mpi_point_t **gamma2;  /** gamma2, for public outcome, size: n*k */
	gcry_mpi_point_t ***gamma3; /** gamma3, for private outcome, size: n*n*k */
	gcry_mpi_point_t **delta2;  /** delta2, for public outcome, size: n*k */
	gcry_mpi_point_t ***delta3; /** delta3, for private outcome, size: n*n*k */
	gcry_mpi_point_t **phi2;    /** phi2, for public outcome, size: n*k */
	gcry_mpi_point_t ***phi3;   /** phi3, for private outcome, size: n*n*k */

	/** proofs for the correctnes of the phi values, size: n*k */
	struct proof_2dle ***phiproofs2;

	/** proofs for the correctnes of the phi values, size: n*n*k */
	struct proof_2dle ***phiproofs3;

	gcry_mpi_point_t *tmpa1;    /** used for temporary storage, size: k */
	gcry_mpi_point_t *tmpb1;    /** used for temporary storage, size: k */

	/** only needed in M+1st price auctions to determine winners */
	struct GNUNET_CRYPTO_EccDlogContext *dlogctx;
};

extern gcry_ctx_t       ec_ctx;
extern gcry_mpi_point_t ec_gen;
extern gcry_mpi_point_t ec_zero;
extern gcry_mpi_t       ec_n;

#endif /* ifndef _BRANDT_INTERNALS_H */
