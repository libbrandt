/* This file is part of libbrandt.
 * Copyright (C) 2016 GNUnet e.V.
 *
 * libbrandt is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * libbrandt is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * libbrandt.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file test.h
 * @brief Some helpers for running tests
 * @author Markus Teich
 */
#ifndef _BRANDT_TEST_H
#define _BRANDT_TEST_H

#include <stdio.h>

int tests_run = 0;
int ret = 0;

#define CHECK(cond, message) do { if (!(cond)) { fputs (message, stderr); fputc ( \
									                 '\n', \
									                 stderr); return 0; \
								  } } while (0)
#define RUN(test) do { if (!test ()) { ret = 1; } } while (0)

#endif // ifndef _BRANDT_TEST_H
