/* This file is part of libbrandt.
 * Copyright (C) 2016 GNUnet e.V.
 *
 * libbrandt is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * libbrandt is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * libbrandt.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file brandt.h
 * @brief This Header defines the external interface of libbrandt.
 * @author Markus Teich
 */

#ifndef _BRANDT_BRANDT_H
#define _BRANDT_BRANDT_H

#include <stdint.h>
#include <unistd.h>

#include <gnunet/gnunet_util_lib.h>

/** opaque, defined in internals.h */
struct BRANDT_Auction;

/** Enumeration of all possible status reports for a single bidder */
enum BRANDT_BidderStatus {
	BRANDT_bidder_won,
};

/**
 * An array of this struct is given to the application by the BRANDT_CbResult()
 * callback. One instance represents the status of a single bidder.
 */
struct BRANDT_Result {
	/** Id of the bidder this instance refers to */
	uint16_t bidder;

	/** The price the bidder has to pay. This value is only set if the #status
	 * indicates the bidder has won. */
	uint16_t price;

	/** Status of the bidder */
	enum BRANDT_BidderStatus status;
};

/**
 * Functions of this type are called by libbrandt when the auction should be
 * started as a seller. The application has to broadcast the ordered list of all
 * bidders to the bidders and must return the amount of bidders to libbrandt.
 * After this function is called no more new bidders may be accepted by the
 * application.
 *
 * @param[in] auction_closure Closure pointer representing the respective
 * auction. This is the Pointer given to BRANDT_new().
 * @return The amount of bidders participating in the auction.
 */
typedef uint16_t
(*BRANDT_CbStart)(void *auction_closure);


/**
 * Functions of this type are called by libbrandt to deliver messages to other
 * participants of an auction. There are two variants how this Callback needs to
 * be implemented. The first is delivering messages as unicast directly to the
 * seller, the second is delivering messages as broadcast to all participants
 * (bidders + seller). All messages need to be authenticated and encrypted
 * before sending and the signature needs to be checked immediately after
 * receipt.
 *
 * @param[in] auction_closure Closure pointer representing the respective
 * auction. This is the Pointer given to BRANDT_join() / BRANDT_new().
 * @param[in] msg The message to be delivered
 * @param[in] msg_len The length of the message @a msg in byte.
 * @return 0 on success, -1 on failure.
 */
typedef int
(*BRANDT_CbDeliver)(void       *auction_closure,
                    const void *msg,
                    size_t     msg_len);


/**
 * Functions of this type are called by libbrandt to report the auction outcome
 * or malicious/erroneous participants.
 *
 * \todo: export *proof* of erroneous behaviour / outcome.
 *
 * @param[in] auction_closure Closure pointer representing the respective
 * auction. This is the Pointer given to BRANDT_join() / BRANDT_new().
 * @param[in] results An array of results for one or more bidders. Each bidder
 * will only be listed once. Misbehaving bidder results and auction completion
 * results are not mixed.
 * @param[in] results_len Amount of items in @a results.
 */
typedef void
(*BRANDT_CbResult)(void                 *auction_closure,
                   struct BRANDT_Result results[],
                   uint16_t             results_len);


void
BRANDT_init ();


/**
 * Parse an auction description data block. See BRANDT_new()
 * for an explanation of the different auction description fields.
 *
 * @param[in] auction_desc The auction description blob published by the seller.
 * @param[in] auction_desc_len Length of @a auction_desc in byte.
 * @param[out] time_start Starting time of the auction. May be NULL.
 * @param[out] time_round Maximum round time of the auction. May be NULL.
 * @param[out] num_prices Amount of possible prices. May be NULL.
 * @param[out] m Auction mode. May be NULL.
 * @param[out] outcome_public Outcome setting. May be NULL.
 * @return 0 on success, -1 on failure.
 */
int
BRANDT_parse_desc (const void                  *auction_desc,
                   size_t                      auction_desc_len,
                   struct GNUNET_TIME_Absolute *time_start,
                   struct GNUNET_TIME_Relative *time_round,
                   uint16_t                    *num_prices,
                   uint16_t                    *m,
                   uint16_t                    *outcome_public);


/**
 * Join an auction described by the @a auction_desc parameter.
 *
 * @param[in] result Pointer to the result callback function
 * @param[in] broadcast Pointer to the broadcast callback function
 * @param[in] unicast Pointer to the unicast callback function
 * @param[in] auction_closure Closure pointer representing the auction. This
 * will not be touched by libbrandt itself. It is only passed to the callbacks.
 * @param[in] auction_desc The auction information data published by the seller.
 * This is opaque to the application. Its content will be parsed. The
 * application MUST check the signature on this data block before passing it to
 * libbrandt!
 * @param[in] auction_desc_len The length in byte of the @a auction_desc
 * structure.
 * @param[in] bid How much to bid on this auction.
 * @param[in] dlogctx The discrete log context obtained from
 * GNUNET_CRYPTO_ecc_dlog_prepare(). Only needed for M+1st price auctions.
 * @return A pointer, which should only be remembered and passed to
 * libbrandt functions when the client needs to refer to this auction. This is a
 * black-box pointer, do NOT dereference/change it or the data it points to!
 */
struct BRANDT_Auction *
BRANDT_join (BRANDT_CbResult                     result,
             BRANDT_CbDeliver                    broadcast,
             BRANDT_CbDeliver                    unicast,
             void                                *auction_closure,
             const void                          *auction_desc,
             size_t                              auction_desc_len,
             uint16_t                            bid,
             struct GNUNET_CRYPTO_EccDlogContext *dlogctx);


/* \todo: have cancellation (BRANDT_join_cancel()) */
/**
 * Create a new auction described by the @a auction_desc parameter as seller.
 *
 * @param[in] result Pointer to the result callback function
 * @param[in] broadcast Pointer to the broadcast callback function
 * @param[in] start Pointer to the seller start callback function
 * @param[in] auction_closure Closure pointer representing the auction. This
 * will not be touched by libbrandt. It is only passed to the callbacks.
 * @param[out] auction_desc The auction information data as an opaque data
 * structure. It is generated by this function and should be distributed to
 * all possibly interested bidders. The seller MUST sign this data block before
 * publishing it!
 * @param[out] auction_desc_len The length in byte of the @a auction_desc
 * structure. Will be filled by BRANDT_new().
 * @param[in] time_start The time when the auction will start. Bidders have
 * until then to register.
 * @param[in] time_round The maximum duration of each round in the protocol.
 * @param[in] num_prices The amount of possible valuations for the sold item(s).
 * Must be > 0.
 * @param[in] m The mode of the auction. If 0, it will be a first price auction
 * where the winner has to pay the price of his bid. If >0 it will be a second
 * price auction selling exactly that amount of items and each winner has to pay
 * the price of the highest loosing bid.
 * @param[in] outcome_public If 1, the auction winner and price will be public
 * to all participants, if 0, this information will only be revealed to the
 * winner and the seller.
 * @param[in] dlogctx The discrete log context obtained from
 * GNUNET_CRYPTO_ecc_dlog_prepare(). Only needed for M+1st price auctions.
 * @return If invalid parameters are passed, NULL is returned. Else the return
 * value is a pointer, which should only be remembered and passed to
 * libbrandt functions when the client needs to refer to this auction. This is a
 * black-box pointer, do NOT dereference/change it or the data it points to!
 */
struct BRANDT_Auction *
BRANDT_new (BRANDT_CbResult                     result,
            BRANDT_CbDeliver                    broadcast,
            BRANDT_CbStart                      start,
            void                                *auction_closure,
            void                                **auction_desc,
            size_t                              *auction_desc_len,
            struct GNUNET_TIME_Absolute         time_start,
            struct GNUNET_TIME_Relative         time_round,
            uint16_t                            num_prices,
            uint16_t                            m,
            int                                 outcome_public,
            struct GNUNET_CRYPTO_EccDlogContext *dlogctx);


/**
 * This function must be called when bidding after receipt of the start
 * notification from the seller.
 *
 * @param[in] auction The pointer returned by BRANDT_join().
 * @param[in] i The index of this bidder assigned by the seller (from the start
 * announcement message).
 * @param[in] n The amount of bidders (from the start announcement message).
 */
void
BRANDT_bidder_start (struct BRANDT_Auction *auction,
                     uint16_t              i,
                     uint16_t              n);


/**
 * Clean up this auction on shutdown.
 *
 * @param[in] auction The pointer returned by BRANDT_join() or BRANDT_new().
 */
void
BRANDT_destroy (struct BRANDT_Auction *auction);

/**
 * Receive a message related to a specific auction.
 *
 * If the message is from a future round and cannot be processed yet, it is
 * cached in RAM. It will not be stored on disc until it can be used, since the
 * messages completing the previous round should all arrive relatively soon.
 *
 * @param[in] auction The pointer returned by BRANDT_join() or BRANDT_new() from
 * which message @a msg was received.
 * @param[in] sender The id of the sender.
 * @param[in] msg The message that was received.
 * @param[in] msg_len The length in byte of @a msg.
 */
void
BRANDT_got_message (struct BRANDT_Auction *auction,
                    uint16_t              sender,
                    const unsigned char   *msg,
                    size_t                msg_len);


/**\todo: Error handling functions? */
/* \todo: implement (de)serialization / persistent caching on disk */

#endif /* ifndef _BRANDT_BRANDT_H */
