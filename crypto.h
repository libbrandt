/* This file is part of libbrandt.
 * Copyright (C) 2016 GNUnet e.V.
 *
 * libbrandt is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * libbrandt is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * libbrandt.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file crypto.h
 * @brief Interface of the crypto primitives.
 * @author Markus Teich
 */

#ifndef _BRANDT_CRYPTO_H
#define _BRANDT_CRYPTO_H

#include <gcrypt.h>
#include <stdint.h>

#include <gnunet/gnunet_util_lib.h>

#include "internals.h"

void brandt_crypto_init ();


/* --- EC --- */

/* used for serialized mpis and serialized curve points (they are the same size
 * when compressed) */
struct ec_mpi {
	unsigned char data[256 / 8];
};

void ec_point_serialize (struct ec_mpi *dst, const gcry_mpi_point_t src);
void ec_point_parse (gcry_mpi_point_t dst, const struct ec_mpi *src);

gcry_mpi_point_t *smc_init1 (uint16_t size1);
gcry_mpi_point_t **smc_init2 (uint16_t size1, uint16_t size2);
gcry_mpi_point_t ***smc_init3 (uint16_t size1, uint16_t size2, uint16_t size3);
void smc_free1 (gcry_mpi_point_t *dst, uint16_t size1);
void smc_free2 (gcry_mpi_point_t **dst, uint16_t size1, uint16_t size2);
void smc_free3 (gcry_mpi_point_t ***dst,
                uint16_t         size1,
                uint16_t         size2,
                uint16_t         size3);

void smc_sums_partial (gcry_mpi_point_t out[],
                       gcry_mpi_point_t in[],
                       uint16_t         len,
                       uint16_t         stepi,
                       uint16_t         stepo);
void smc_sum (gcry_mpi_point_t out,
              gcry_mpi_point_t in[],
              uint16_t         len,
              uint16_t         step);

void ec_point_copy (gcry_mpi_point_t dst, const gcry_mpi_point_t src);
int ec_point_cmp (const gcry_mpi_point_t a, const gcry_mpi_point_t b);
void ec_skey_create (gcry_mpi_t skey);
void ec_keypair_create (gcry_mpi_point_t pkey, gcry_mpi_t skey);
void ec_keypair_create_base (gcry_mpi_point_t       pkey,
                             gcry_mpi_t             skey,
                             const gcry_mpi_point_t base);


/* --- Zero knowledge proofs --- */

struct proof_dl {
	struct ec_mpi r;
	struct ec_mpi a;
};

struct proof_2dle {
	struct ec_mpi r;
	struct ec_mpi a;
	struct ec_mpi b;
};

struct proof_0og {
	struct ec_mpi a1;
	struct ec_mpi a2;
	struct ec_mpi b1;
	struct ec_mpi b2;
	struct ec_mpi d1;
	struct ec_mpi d2;
	struct ec_mpi r1;
	struct ec_mpi r2;
};

void smc_zkp_dl (gcry_mpi_point_t v,
                 const gcry_mpi_t x,
                 struct proof_dl  *proof);
int smc_zkp_dl_check (const gcry_mpi_point_t v,
                      const struct proof_dl  *proof);

void smc_zkp_2dle (const gcry_mpi_point_t v,
                   const gcry_mpi_point_t w,
                   const gcry_mpi_point_t g1,
                   const gcry_mpi_point_t g2,
                   const gcry_mpi_t       x,
                   struct proof_2dle      *proof);
int smc_zkp_2dle_check (const gcry_mpi_point_t  v,
                        const gcry_mpi_point_t  w,
                        const gcry_mpi_point_t  g1,
                        const gcry_mpi_point_t  g2,
                        const struct proof_2dle *proof);

void smc_zkp_0og (int                    m_is_gen,
                  const gcry_mpi_point_t y,
                  gcry_mpi_t             r,
                  gcry_mpi_point_t       alpha,
                  gcry_mpi_point_t       beta,
                  struct proof_0og       *proof);
int smc_zkp_0og_check (const gcry_mpi_point_t y,
                       const gcry_mpi_point_t alpha,
                       const gcry_mpi_point_t beta,
                       const struct proof_0og *proof);


/* --- Protocol implementation --- */

void smc_prep_keyshare (struct BRANDT_Auction *ad);
unsigned char *smc_gen_keyshare (struct BRANDT_Auction *ad, size_t *buflen);
int smc_recv_keyshare (struct BRANDT_Auction *ad,
                       const unsigned char   *buf,
                       size_t                buflen,
                       uint16_t              sender_index);

void smc_prep_bid (struct BRANDT_Auction *ad);
unsigned char *smc_encrypt_bid (struct BRANDT_Auction *ad, size_t *buflen);
int smc_recv_encrypted_bid (struct BRANDT_Auction *ad,
                            const unsigned char   *buf,
                            size_t                buflen,
                            uint16_t              sender_index);

void fp_priv_prep_outcome (struct BRANDT_Auction *ad);
unsigned char *fp_priv_compute_outcome (struct BRANDT_Auction *ad,
                                        size_t                *buflen);
int fp_priv_recv_outcome (struct BRANDT_Auction *ad,
                          const unsigned char   *buf,
                          size_t                buflen,
                          uint16_t              sender);

void fp_priv_prep_decryption (struct BRANDT_Auction *ad);
unsigned char *fp_priv_decrypt_outcome (struct BRANDT_Auction *ad,
                                        size_t                *buflen);
int fp_priv_recv_decryption (struct BRANDT_Auction *ad,
                             const unsigned char   *buf,
                             size_t                buflen,
                             uint16_t              sender);

struct BRANDT_Result *fp_priv_determine_outcome (struct BRANDT_Auction *ad,
                                                 uint16_t              *len);

void fp_pub_prep_outcome (struct BRANDT_Auction *ad);
unsigned char *fp_pub_compute_outcome (struct BRANDT_Auction *ad,
                                       size_t                *buflen);
int fp_pub_recv_outcome (struct BRANDT_Auction *ad,
                         const unsigned char   *buf,
                         size_t                buflen,
                         uint16_t              sender);

void fp_pub_prep_decryption (struct BRANDT_Auction *ad);
unsigned char *fp_pub_decrypt_outcome (struct BRANDT_Auction *ad,
                                       size_t                *buflen);
int fp_pub_recv_decryption (struct BRANDT_Auction *ad,
                            const unsigned char   *buf,
                            size_t                buflen,
                            uint16_t              sender);

struct BRANDT_Result *fp_pub_determine_outcome (struct BRANDT_Auction *ad,
                                                uint16_t              *len);


void mp_priv_prep_outcome (struct BRANDT_Auction *ad);

struct BRANDT_Result *mp_priv_determine_outcome (struct BRANDT_Auction *ad,
                                                 uint16_t              *len);


void mp_pub_prep_outcome (struct BRANDT_Auction *ad);
unsigned char *mp_pub_compute_outcome (struct BRANDT_Auction *ad,
                                       size_t                *buflen);
int mp_pub_recv_outcome (struct BRANDT_Auction *ad,
                         const unsigned char   *buf,
                         size_t                buflen,
                         uint16_t              sender);

void mp_pub_prep_decryption (struct BRANDT_Auction *ad);
unsigned char *mp_pub_decrypt_outcome (struct BRANDT_Auction *ad,
                                       size_t                *buflen);
int mp_pub_recv_decryption (struct BRANDT_Auction *ad,
                            const unsigned char   *buf,
                            size_t                buflen,
                            uint16_t              sender);

struct BRANDT_Result *mp_pub_determine_outcome (struct BRANDT_Auction *ad,
                                                uint16_t              *len);


/* --- Round dictionaries --- */

typedef void
(*RoundPrep)(struct BRANDT_Auction *ad);

typedef int
(*MsgIn)(struct BRANDT_Auction *ad,
         const unsigned char   *buf,
         size_t                buflen,
         uint16_t              sender);


typedef unsigned char *
(*MsgOut)(struct BRANDT_Auction *ad,
          size_t                *buflen);


/**
 * Functions of this type determine the outcome of an auction.
 *
 * \todo: export *proof* of erroneous behaviour / outcome.
 *
 * @param[in] ad Pointer to the auction data struct.
 * @param[out] len Amount of items in @a results.
 * @return An array of results for one or more bidders. Each bidder
 * will only be listed once.
 */
typedef struct BRANDT_Result *
(*Result)(struct BRANDT_Auction *ad,
          uint16_t              *len);


static const RoundPrep handler_prep[auction_last][outcome_last][msg_last] = {
	[auction_firstPrice] =      {
		[outcome_private] =     {
			[msg_init] = &smc_prep_keyshare,
			[msg_bid] = &smc_prep_bid,
			[msg_outcome] = &fp_priv_prep_outcome,
			[msg_decrypt] = &fp_priv_prep_decryption,
		},
		[outcome_public] =      {
			[msg_init] = &smc_prep_keyshare,
			[msg_bid] = &smc_prep_bid,
			[msg_outcome] = &fp_pub_prep_outcome,
			[msg_decrypt] = &fp_pub_prep_decryption,
		},
	},
	[auction_mPlusFirstPrice] = {
		[outcome_private] =     {
			[msg_init] = &smc_prep_keyshare,
			[msg_bid] = &smc_prep_bid,
			[msg_outcome] = &mp_priv_prep_outcome,
			[msg_decrypt] = &fp_priv_prep_decryption,
		},
		[outcome_public] =      {
			[msg_init] = &smc_prep_keyshare,
			[msg_bid] = &smc_prep_bid,
			[msg_outcome] = &mp_pub_prep_outcome,
			[msg_decrypt] = &mp_pub_prep_decryption,
		},
	},
};


/**
 * stores the function pointers to receive functions for each state.
 *
 * The first index denotes if a first price auction or a M+1st price auction is
 * used. If it is 0, it is a first price auction, if it is 1, it is a M+1st
 * price auction.
 *
 * The second index denotes if the outcome should be public or private. A value
 * of 0 means a private outcome, while a value of 1 means public outcome.
 */
static const MsgIn handler_in[auction_last][outcome_last][msg_last] = {
	[auction_firstPrice] =      {
		[outcome_private] =     {
			[msg_init] = &smc_recv_keyshare,
			[msg_bid] = &smc_recv_encrypted_bid,
			[msg_outcome] = &fp_priv_recv_outcome,
			[msg_decrypt] = &fp_priv_recv_decryption,
		},
		[outcome_public] =      {
			[msg_init] = &smc_recv_keyshare,
			[msg_bid] = &smc_recv_encrypted_bid,
			[msg_outcome] = &fp_pub_recv_outcome,
			[msg_decrypt] = &fp_pub_recv_decryption,
		},
	},
	[auction_mPlusFirstPrice] = {
		[outcome_private] =     {
			[msg_init] = &smc_recv_keyshare,
			[msg_bid] = &smc_recv_encrypted_bid,
			[msg_outcome] = &fp_priv_recv_outcome,
			[msg_decrypt] = &fp_priv_recv_decryption,
		},
		[outcome_public] =      {
			[msg_init] = &smc_recv_keyshare,
			[msg_bid] = &smc_recv_encrypted_bid,
			[msg_outcome] = &mp_pub_recv_outcome,
			[msg_decrypt] = &mp_pub_recv_decryption,
		},
	},
};


/**
 * stores the function pointers to message buffer creating functions for each
 * state.
 *
 * The first index denotes if a first price auction or a M+1st price auction is
 * used. If it is 0, it is a first price auction, if it is 1, it is a M+1st
 * price auction.
 *
 * The second index denotes if the outcome should be public or private. A value
 * of 0 means a private outcome, while a value of 1 means public outcome.
 */
static const MsgOut handler_out[auction_last][outcome_last][msg_last] = {
	[auction_firstPrice] =      {
		[outcome_private] =     {
			[msg_init] = &smc_gen_keyshare,
			[msg_bid] = &smc_encrypt_bid,
			[msg_outcome] = &fp_priv_compute_outcome,
			[msg_decrypt] = &fp_priv_decrypt_outcome,
		},
		[outcome_public] =      {
			[msg_init] = &smc_gen_keyshare,
			[msg_bid] = &smc_encrypt_bid,
			[msg_outcome] = &fp_pub_compute_outcome,
			[msg_decrypt] = &fp_pub_decrypt_outcome,
		},
	},
	[auction_mPlusFirstPrice] = {
		[outcome_private] =     {
			[msg_init] = &smc_gen_keyshare,
			[msg_bid] = &smc_encrypt_bid,
			[msg_outcome] = &fp_priv_compute_outcome,
			[msg_decrypt] = &fp_priv_decrypt_outcome,
		},
		[outcome_public] =      {
			[msg_init] = &smc_gen_keyshare,
			[msg_bid] = &smc_encrypt_bid,
			[msg_outcome] = &mp_pub_compute_outcome,
			[msg_decrypt] = &mp_pub_decrypt_outcome,
		},
	},
};

/**
 * stores the function pointers to outcome determination functions for each
 * auction mode.
 *
 * The first index denotes if a first price auction or a M+1st price auction is
 * used. If it is 0, it is a first price auction, if it is 1, it is a M+1st
 * price auction.
 *
 * The second index denotes if the outcome should be public or private. A value
 * of 0 means a private outcome, while a value of 1 means public outcome.
 */
static const Result handler_res[auction_last][outcome_last] = {
	[auction_firstPrice] =      {
		[outcome_private] = &fp_priv_determine_outcome,
		[outcome_public] = &fp_pub_determine_outcome,
	},
	[auction_mPlusFirstPrice] = {
		[outcome_private] = &mp_priv_determine_outcome,
		[outcome_public] = &mp_pub_determine_outcome,
	},
};



#endif /* ifndef _BRANDT_CRYPTO_H */
