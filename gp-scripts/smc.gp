/* search for a random prime with the specified amount of bits */
\\ randomprime(bits:small=127)=
\\ {
\\ 	local(r:int=4);
\\ 	while(!isprime(r),
\\ 		r = bitor(2^(bits-1) + random(2^(bits-1)-1), 1);
\\ 	);
\\ 	r;
\\ }

smc_hextodec(s:str) =
{
	local(v:vecsmall = Vecsmall(s), ret:int = 0);
	for(i = 1, #v,
		ret = (ret<<4) + if(v[i]<=57 && v[i]>=48, v[i]-48, v[i]<=70 && v[i]>=65, v[i]-55, v[i]<=102 && v[i]>=97, v[i]-87, error("invalid input format"))
	);
	ret;
}

