\\ From: "Fully private auctions in a constant number of rounds" (2003) by Felix Brandt pages 9-10


\\\\\\\\\\\\
\\ Adapt the following values to your needs
\\\\\\\\\\\\

\\ auction parameter
M = 1
\\ amount of bidders
n = 2^3
\\ amount of possible prices
k = 2^7
\\ randomize bids (change to something static, if you like)
bid = vector(n,i,random(k)+1)
\\bid = vector(n,i,n-i+1)     \\ first bidder wins
\\bid = vector(n,i,i)         \\ last bidder wins
\\bid = vector(n,i,(i+1)%2)   \\ second bidder wins (with ties)

\\ prime finite field setup (result may be ambiguous if your prime is too small, 4*n*k seems to work fine)
\\p = 263
\\q = (p-1)/2
\\ use save prime p
q = prime(2^12)
p = 2*q + 1

\\ get generator / primitive element for G_q
\\var = 'x                                                    \\ copy pasta from internet
\\pe=ffgen(minpoly(ffprimroot(ffgen(ffinit(p,1))),var),var)   \\ get primitive element
\\1/(fforder(pe) == p-1)                                      \\ error out, if ord(pe) is wrong
\\g = Mod(eval(Str(pe))^2, p)                                 \\ dirty hack to convert t_FFELEM to t_INT
g = Mod(4, p)

\\\\\\\\\\\\
\\ PROLOG
\\\\\\\\\\\\

\\ private keys of agents
x = vector(n,i,random(q))
\\ public keyshares of agents
yshares = vector(n,i,g^x[i])
\\ shared public key
y = prod(X=1,n,yshares[X])

\\ first index level = owning agent id (additive share)
\\ second index level = agent id, price id
m = vector(n,i,matrix(n,k,a,b,random(q)))

\\ index = owning agent id, price id
r = matrix(n,k,i,j,random(q))
\\ bid matrix
b = matrix(n,k,i,j,g^(bid[i]==j))

\\\\\\\\\\\\
\\ ROUND1
\\\\\\\\\\\\

\\ encrypted bids
alpha = matrix(n,k,i,j, b[i,j]*y^r[i,j])
beta  = matrix(n,k,i,j,        g^r[i,j])

\\\\\\\\\\\\
\\ ROUND2
\\\\\\\\\\\\

\\ multiplicative shares
\\ first index level = owning agent id (multiplicative share)
\\ second index level = agent id, price id
Gamma = vector(n,a,matrix(n,k,i,j, ( prod(h=1,n,prod(d=j,k,alpha[h,d]) * prod(d=j+1,k,alpha[h,d])) * prod(d=1,j,alpha[i,d])^(2*M+2) / g^(2*M+1) )^(m[a][i,j])        ))
Delta = vector(n,a,matrix(n,k,i,j, ( prod(h=1,n,prod(d=j,k, beta[h,d]) * prod(d=j+1,k, beta[h,d])) * prod(d=1,j, beta[i,d])^(2*M+2) )^(m[a][i,j]) ))

\\\\\\\\\\\\
\\ ROUND3
\\\\\\\\\\\\

\\ multiplicative shares (decryption)
\\ first index level = owning agent id (multiplicative share)
\\ second index level = agent id, price id
Phi = vector(n,a,matrix(n,k,i,j, prod(h=1,n,Delta[h][i,j])^x[a] ))

\\\\\\\\\\\\
\\ EPILOG
\\\\\\\\\\\\

\\ winner matrix
v = matrix(n,k,a,j, prod(i=1,n,Gamma[i][a,j]) / prod(i=1,n,Phi[i][a,j]) )
vi = lift(v)

print("bids are: ", bid)
for(X=1,n, if(vecmin(vi[X,],&i)==1, print("And the winner is ", X, " with the price ", i) ))
